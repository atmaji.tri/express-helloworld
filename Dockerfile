FROM node:lts-alpine3.20

ARG USER=default
ENV HOME /home/$USER

# install sudo as root
RUN apk add --update sudo

# add new user
RUN adduser -D $USER \
        && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER \
        && chmod 0440 /etc/sudoers.d/$USER 

WORKDIR $HOME

COPY . $HOME

RUN npm install

RUN chown -R $USER:$USER $HOME

USER $USER

EXPOSE 3000

CMD ["node", "index.js"]
