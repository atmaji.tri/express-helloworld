const express = require('express')
const bodyParser = require('body-parser')
const sum = require('./sum');
const app = express()
const port = 3000

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/test', (req, res) => {
  const request = req.body
  const response = sum(request.a, request.b)
  res.send('post hai jest test ' + response)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})